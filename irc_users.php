<?php
/*
 *      irc_users.php - this file is part of vomak - a very simple IRC bot
 *
 *      Copyright 2008 Enrico Tröger <enrico(dot)troeger(at)uvena(dot)de>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

/* This is a sample implementation in PHP to connect to vomak's socket
 * and send/retrieve some data */


function not_empty($var)
{
    return ($var != "");
}


$socket = @socket_create(AF_UNIX, SOCK_STREAM, 0);
if (! $socket)
{
    echo "socket_create() failed:\nreason: " . socket_strerror($socket) . "\n";
}
else
{
    $result = @socket_connect($socket, "/var/tmp/vomak_socket");
    if (! $result)
    {
        echo "socket_connect() failed.\reason: " . socket_strerror($result) . "\n";
    }
    else
    {
        socket_write($socket, "userlist\r\n", 10);
        $users_tmp = socket_read($socket, 2048);
        socket_close($socket);

        $users_tmp = str_replace("@ChanServ", "", $users_tmp);
        $users_tmp = str_replace("GeanyBot", "", $users_tmp);
        // remove op indicator
        $users_tmp = str_replace("@", "", $users_tmp);
        $users = explode(" ", trim($users_tmp));
        //print_r($users);
        $users = array_filter($users, "not_empty");
        $user_count = count($users);

        echo "Currently $user_count users are connected:<br/>";
        foreach ($users as $u)
        {
            echo "$u ";
        }
    }
}
?>
