
.PHONY: build

all: build

build:
	@./waf build

install:
	@./waf install

uninstall:
	@./waf uninstall

clean:
	@./waf clean

distclean:
	@./waf distclean
	@-rm -f Makefile

