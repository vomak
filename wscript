#! /usr/bin/env python
# -*- coding: utf-8 -*-
#
# WAF build script - this file is part of vomak - a very simple IRC bot
#
# Copyright 2008 Enrico Tröger <enrico(dot)troeger(at)uvena(dot)de>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# $Id: wscript 2649 2008-06-05 16:52:39Z eht16 $

"""
This is a WAF build script (http://code.google.com/p/waf/).
"""


import Options, Utils, subprocess


APPNAME = 'vomak'
VERSION = '0.1'

srcdir = '.'
blddir = '_build_'


def configure(conf):

	conf.check_tool('compiler_cc')

	conf.check_cfg(package='glib-2.0', atleast_version='2.6.0', uselib_store='GLIB', mandatory=True)
	conf.check_cfg(package='glib-2.0', args='--cflags --libs', uselib_store='GLIB')

	if Options.options.debug:
		conf.env.append_value('CCFLAGS', '-g -DDEBUG -O0')


def set_options(opt):
	opt.tool_options('compiler_cc')

	opt.add_option('--html', action='store_true', default=False,
		help='generate HTML doc [default: No]', dest='html')

	opt.add_option('--debug', action='store_true', default=False,
		help='enable debug mode [default: No]', dest='debug')


def build(bld):
	obj = bld.new_task_gen('cc', 'program')
	obj.name		  = 'vomak'
	obj.target		  = 'vomak'
	obj.source		  = 'src/irc.c src/socket.c src/vomak.c'
	obj.includes	  = '. src/'
	obj.uselib		  = 'GLIB'


def shutdown():
    if Options.options.html:
        # first try rst2html.py as it is the upstream default
        ret = launch('rst2html.py -stg --stylesheet=vomak.css README readme.html',
            'Generating HTML documentation')
        if not ret == 0:
            launch('rst2html -stg --stylesheet=vomak.css README readme.html',
                    'Generating HTML documentation (using fallback "rst2html")')
        ret = launch('rst2html -stg --stylesheet=vomak.css README readme.html',
            'Generating HTML documentation')


# Simple function to execute a command and print its exit status
def launch(command, status, success_color='GREEN'):
	ret = 0
	Utils.pprint(success_color, status)
	try:
		ret = subprocess.call(command.split())
	except OSError, e:
		ret = 1
		print str(e), ":", command
	except:
		ret = 1

	if ret != 0:
		Utils.pprint('RED', status + ' failed')

	return ret
