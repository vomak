/*
 *      vomak.h - this file is part of vomak - a very simple IRC bot
 *
 *      Copyright 2008 Enrico Tröger <enrico(dot)troeger(at)uvena(dot)de>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; version 2 of the License.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */


#ifndef VOMAK_H
#define VOMAK_H 1


#ifdef DEBUG
#define vdebug(str)				g_message("%s: "#str"=%s", __func__, str)
#define debug(a, varargs...)	g_message(a, ##varargs)
#define TRACE					g_message("Entering: %s", __func__);
#else
#define vdebug(str)
#define debug(a, varargs...)
#define TRACE
#endif


/* Returns: TRUE if @a ptr points to a non-zero value. */
#define NZV(ptr) \
	((ptr) && (ptr)[0])


typedef struct
{
	gchar *socketname; // vomak <-> unix domain socket
	gchar *socketperm;
	gchar *server;
	gint   port;
	gchar *channel;
	gchar *servername;
	gchar *nickname;
	gchar *username;
	gchar *realname;
	gchar *nickserv_password;
	gchar *logfile;
	gchar *fortune_cmd;
	gint query_names_interval;
	gboolean auto_rejoin;
	GHashTable *data;
} config_t;

extern config_t *config;


void help_system_query(const gchar *msg);
gboolean help_system_learn(const gchar *keyword, const gchar *text);
void set_user_list(const gchar *userlist);
const gchar *get_user_list();
void main_quit();

void vomak_warning(gchar const *format, ...) G_GNUC_PRINTF (1, 2);

#endif
