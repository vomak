/*
 *      irc.c - this file is part of vomak - a very simple IRC bot
 *
 *      Copyright 2008 Enrico Tröger <enrico(dot)troeger(at)uvena(dot)de>
 *      Copyright 2008 Dominic Hopf	<dh(at)dmaphy(dot)de>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; version 2 of the License.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>
#include <string.h>
#include <strings.h>
#include <signal.h>
#include <time.h>
#ifndef DEBUG
# include <syslog.h>
#endif

#include <glib.h>
#include <glib/gstdio.h>

#include "vomak.h"
#include "socket.h"
#include "irc.h"


config_t *config;


static void irc_kick(irc_conn_t *irc_conn, const gchar *nickname);
static gboolean irc_is_user_op(irc_conn_t *irc_conn, const gchar *nickname);

enum
{
	GOODIES_COFFEE,
	GOODIES_COKE,
	GOODIES_BEER,
	GOODIES_TEA,
	GOODIES_PIZZA,
};
typedef struct
{
	const gchar *command;
	const gchar *message;
} goodies_t;

const goodies_t goodies[] = {
	{ "!coffee", "A nice sexy waitress brings %s a big cup of coffee!" },
	{ "!coke", "A nice sexy waitress brings %s a cool bottle of coke!" },
	{ "!beer", "A nice sexy waitress brings %s a nice bottle of beer!" },
	{ "!tea", "A nice sexy waitress brings %s a cup of hot tea!" },
	{ "!pizza", "Someone calls Mario, and he brings %s a tasty hawaiian pizza!" },
	{ NULL, NULL }
};

gboolean irc_query_names(gpointer data)
{
	irc_conn_t *irc = data;
	static gchar msg[1024];
	guint msg_len;

	TRACE

	msg_len = g_snprintf(msg, sizeof msg, "NAMES #%s\r\n", config->channel);
	// send the message directly to avoid logging (prevent log file spamming)
	send(irc->socket, msg, msg_len, MSG_NOSIGNAL);

	return TRUE;
}


/* 'line' should be terminated by "\r\n" (CRLF) */
static void irc_log(irc_conn_t *irc_conn, const gchar *line, gboolean send)
{
	time_t t;
	struct tm *tp;
	static gchar str[256];
	GString *log_line;

	if (! irc_conn->log_fd)
		return;

	t = time(NULL);
	tp = localtime(&t);
	strftime(str, sizeof str, "%F %T %z ", tp);

	log_line = g_string_new(str);
	if (send) // if we are sending a message, add our ident string
	{
		g_string_append_printf(log_line, ":%s!n=%s@%s ",
			config->nickname, config->username, config->servername);
	}
	g_string_append(log_line, line);
	/* add \r\n if it is missing */
	if (strncmp(log_line->str + log_line->len - 2, "\r\n", 2) != 0)
	{
		g_string_append(log_line, "\r\n");
	}
	fwrite(log_line->str, log_line->len, 1, irc_conn->log_fd);
	fflush(irc_conn->log_fd);
	g_string_free(log_line, TRUE);
}


static void irc_send_private_message(irc_conn_t *irc_conn, const gchar *target, const gchar *format, ...)
{
	static gchar tmp_msg[1024];
	static gchar msg[1024];
	guint msg_len;
	va_list ap;

	if (target == NULL)
		return;

	va_start(ap, format);
	g_vsnprintf(tmp_msg, sizeof tmp_msg, format, ap);
	va_end(ap);

	msg_len = g_snprintf(msg, sizeof msg, "PRIVMSG %s :%s, %s\r\n", target, target, tmp_msg);
	irc_send(irc_conn, msg, msg_len);
}


static gchar *get_nickname(const gchar *line, guint len)
{
	static gchar result[20];
	guint i, j = 0;

	// :eht16!n=enrico@uvena.de PRIVMSG #eht16 :df
	for (i = 0; i < len; i++)
	{
		if (line[i] == '!' || line[i] == '=' || j >= 19)
			break;

		if (line[i] == ':')
			continue;

		result[j++] = line[i];
	}
	result[j] = '\0';

	return result;
}


static gchar *get_command_sender(const gchar *line, guint len, const gchar *command)
{
	guint i;
	gsize cmd_len = strlen(command);

	// :eht16!n=enrico@uvena.de PRIVMSG GeanyTestBot :hi
	for (i = 0; i < len; i++)
	{
		if (line[i] != ' ')
			continue;

		if (strncmp(command, line + i + 1, cmd_len) == 0)
		{
			static gchar name[20];
			g_snprintf(name, sizeof(name), "%s :", config->nickname);
			// if the receiver of the message is me, then it's a private message and
			// we return the sender's nickname, otherwise NULL
			if (strncmp(name, line + i + cmd_len + 2, strlen(config->nickname) + 2) == 0 ||
				strncmp("KICK", command, 4) == 0)
			{
				return get_nickname(line, len);
			}
			else
				return NULL;
		}
	}

	return NULL;
}


static gint get_response(const gchar *line, guint len)
{
	static gchar result[4];
	guint i, j = 0;
	gboolean in_response = FALSE;

	// :kornbluth.freenode.net 353 GeanyTestBot @ #eht16 :GeanyTestBot eht16
	// :kornbluth.freenode.net 366 GeanyTestBot #eht16 :End of /NAMES list.
	for (i = 0; i < len; i++)
	{
		// before a response code
		if (line[i] != ' ' && ! in_response)
			continue;

		// after a response code
		if (line[i] == ' ' && in_response)
		{
			in_response = FALSE;
			break;
		}

		if (line[i] == ' ' )
			i++; // skip the space

		result[j++] = line[i];
		in_response = TRUE;
	}
	result[j] = '\0';

	return atoi(result);
}


const gchar *irc_get_message(const gchar *line, guint len)
{
	static gchar result[1024] = { 0 };
	guint i, j = 0;
	gint state = 0; // represents the current part of the whole line, separated by spaces

	// :eht16!n=enrico@uvena.de PRIVMSG #eht16 :df foo: var
	// -> df foo: var
	for (i = 0; i < len; i++)
	{
		if (state < 3)
		{
			if (line[i] == ' ')
			{
				state++;
				i++; // skip the ':'
				continue;
			}
		}
		else if (line[i] != '\r' && line[i] != '\n')
		{
			result[j++] = line[i];
		}
	}
	result[j] = '\0';

	return result;
}


static const gchar *irc_get_message_with_name(const gchar *line, guint len, const gchar *name)
{
	const gchar *tmp;
	gsize name_len;

	tmp = irc_get_message(line, len);
	name_len = strlen(name);

	if (strncmp(tmp, name, name_len) == 0)
		tmp += name_len;

	return tmp;
}


// returns a nickname argument given to cmd, it may also be "cmd for nickname", then the "for" is
// skipped
static const gchar *get_argument_target(const gchar *line, guint len, const gchar *cmd)
{
	static gchar result[20];
	const gchar *tmp;
	gchar **parts;

	// :eht16!n=enrico@uvena.de PRIVMSG #eht16 :!beer for me
	tmp = irc_get_message(line, len);

	// -> !beer for me
	parts = g_strsplit(tmp, " ", -1);
	if (parts == NULL || parts[0] == NULL)
	{
		g_strfreev(parts);
		return NULL;
	}

	// if cmd doesn't match, skip it
	if (parts[0] != NULL && strcmp(parts[0], cmd) != 0)
	{
		g_strfreev(parts);
		return NULL;
	}

	if (parts[1] == NULL)
	{
		g_strfreev(parts);
		return NULL;
	}

	if (strcmp("for", parts[1]) == 0)
	{
		if (parts[2] != NULL)
		{
			if (strcmp(parts[2], "me") == 0)
			{
				g_strfreev(parts);
				return NULL; // if we return NULL, the nickname of the caller is used, aka "me"
			}
			else
				g_strlcpy(result, parts[2], sizeof(result));
		}
		else
			g_strlcpy(result, parts[1], sizeof(result));
	}
	else if (strcmp(parts[1], "me") == 0)
	{
		g_strfreev(parts);
		return NULL; // if we return NULL, the nickname of the caller is used, aka "me"
	}
	else
	{
		g_strlcpy(result, parts[1], sizeof(result));
	}

	g_strfreev(parts);
	return result;
}


// Parses the line and puts the first argument in arg1, all further arguments are concatenated
// in arg2. arg1 and arg2 should be freed when no longer needed.
// If arg1 and arg2 were set successfully, TRUE is returned, if any error occurs, FALSE is returned
// and arg1 and arg2 are set to NULL.
static gboolean get_argument_two(const gchar *line, guint len, const gchar *cmd,
								 gchar **arg1, gchar **arg2)
{
	const gchar *tmp;
	gchar **parts;

	*arg1 = NULL;
	*arg2 = NULL;

	// :eht16!n=enrico@uvena.de PRIVMSG #eht16 :!learn keyword text to be added
	tmp = irc_get_message(line, len);

	// -> !learn keyword text to be added
	parts = g_strsplit(tmp, " ", 3);
	if (parts == NULL || parts[0] == NULL)
	{
		g_strfreev(parts);
		return FALSE;
	}

	// if cmd doesn't match, skip it
	if (parts[0] != NULL && strcmp(parts[0], cmd) != 0)
	{
		g_strfreev(parts);
		return FALSE;
	}

	if (parts[1] == NULL || parts[2] == NULL)
	{
		g_strfreev(parts);
		return FALSE;
	}

	*arg1 = g_strdup(parts[1]);
	*arg2 = g_strdup(parts[2]);

	g_strfreev(parts);

	return TRUE;
}


static void command_fortune(irc_conn_t *irc_conn)
{
	GError *error = NULL;
	gchar *out = NULL;
	gchar *err = NULL;

	if (g_spawn_command_line_sync(config->fortune_cmd, &out, &err, NULL, &error))
	{
		if (NZV(out))
		{
			gchar **lines = g_strsplit(out, "\n", -1);
			gsize i, len;

			len = g_strv_length(lines);
			for (i = 0; i < len; i++)
			{
				if (strlen(g_strchomp(lines[i])) > 0)
					irc_send_message(irc_conn, NULL, lines[i]);
			}
			g_strfreev(lines);
		}
		else
		{
			vomak_warning("Executing fortune failed (%s)", err);
		}
		g_free(out);
		g_free(err);
	}
	else
	{
		vomak_warning("Executing fortune failed (%s)", error->message);
		g_error_free(error);
	}
}


static void command_moo(irc_conn_t *irc_conn)
{
	gint32 rand = g_random_int();

	if (rand % 2 == 0)
	{
		gsize i;
		const gchar *moo_str[] = {
				"         ^__^\r\n",
				"         (oo)\r\n",
				"   /-----(__)\r\n",
				"  / |    ||\r\n",
				" *  /\\---/\\\r\n",
				"    ~~   ~~\n\r\n",
				"..\"Have you mooed today?\"..\r\n",
				NULL
		};

		for (i = 0; moo_str[i] != NULL; i++)
		{
			irc_send_message(irc_conn, NULL, moo_str[i]);
		}
	}
	else
	{
		irc_send_message(irc_conn, NULL, "I have Super Cow Powers. Have you mooed today?");
	}
}


static void command_learn(irc_conn_t *irc_conn, const gchar *line, guint len)
{
	gchar *arg1, *arg2;

	if (get_argument_two(line, len, "!learn", &arg1, &arg2))
	{
		gint result = help_system_learn(arg1, arg2);
		gchar *text;

		switch (result)
		{
			case 0:
			{
				text = g_strdup_printf("new keyword \"%s\" was added.", arg1);
				break;
			}
			case 1:
			{
				text = g_strdup_printf("existing keyword \"%s\" was updated.", arg1);
				break;
			}
			default:
			{
				text = g_strdup("an error occurred. Database not updated.");
				break;
			}
		}
		irc_send_message(irc_conn, get_nickname(line, len), "%s", text);

		g_free(text);
		g_free(arg1);
		g_free(arg2);
	}
	else
		irc_send_message(irc_conn, get_nickname(line, len),
			"wrong usage of !learn. Use \"?? learn\" for usage information.");
}


static void command_alias(irc_conn_t *irc_conn, const gchar *line, guint len)
{
	gchar *arg1, *arg2;

	if (get_argument_two(line, len, "!alias", &arg1, &arg2))
	{
		// detect if arg2 has more than one word by scanning for spaces in
		// the string
		if (strchr(arg2, ' '))
		{
			irc_send_message(irc_conn, get_nickname(line, len),
				"You gave me more than two arguments for !alias. I can not handle this.");
		}
		// check if the target actually exist and refuse if it doesn't exist
		else if (g_hash_table_lookup(config->data, arg2) == NULL)
		{
			irc_send_message(irc_conn, get_nickname(line, len),
				"The given target for the alias does not exist. I will refuse your request.");
		}
		else
		{
			gint result;
			gchar *text;
			gchar *alias = g_strconcat("@", arg2, NULL);

			result = help_system_learn(arg1, alias);

			switch (result)
			{
				case 0:
				{
					text = g_strdup_printf("new alias \"%s\" was added.", arg1);
					break;
				}
				case 1:
				{
					text = g_strdup_printf("existing alias \"%s\" was updated.", arg1);
					break;
				}
				default:
				{
					text = g_strdup("An error occurred. Database not updated.");
					break;
				}
			}

			irc_send_message(irc_conn, get_nickname(line, len), "%s", text);

			g_free(alias);
			g_free(text);
			g_free(arg1);
			g_free(arg2);
		}
	}
	else
		irc_send_message(irc_conn, get_nickname(line, len),
			"wrong usage of !alias. Use \"?? alias\" for usage information.");
}


static void command_goodies(irc_conn_t *irc_conn, const gchar *line, guint len, gint goodie)
{
	const goodies_t *g = &goodies[goodie];
	const gchar *arg = get_argument_target(line, len, g->command);

	if (arg == NULL)
		arg = get_nickname(line, len);

	irc_send_message(irc_conn, NULL,
		g->message, arg);
}


static void process_command(irc_conn_t *irc_conn, const gchar *line, guint len, const gchar *content)
{
 	// !test
	if (strncmp(content, "!test", 5) == 0)
	{
		irc_send_message(irc_conn, get_nickname(line, len), "I don't like tests!");
	}
	// !moo
	else if (strncmp(content, "!moo", 4) == 0)
	{
		command_moo(irc_conn);
	}
	// !fortune
	else if (config->fortune_cmd != NULL && strncmp(content, "!fortune", 8) == 0)
	{
		command_fortune(irc_conn);
	}
	// !coffee
	else if (strncmp(content, "!coffee", 7) == 0)
	{
		command_goodies(irc_conn, line, len, GOODIES_COFFEE);
	}
	// !coke
	else if (strncmp(content, "!coke", 5) == 0)
	{
		command_goodies(irc_conn, line, len, GOODIES_COKE);
	}
	// !beer
	else if (strncmp(content, "!beer", 5) == 0)
	{
		command_goodies(irc_conn, line, len, GOODIES_BEER);
	}
	// !pizza
	else if (strncmp(content, "!pizza", 5) == 0)
	{
		command_goodies(irc_conn, line, len, GOODIES_PIZZA);
	}
	// !tea
	else if (strncmp(content, "!tea", 5) == 0)
	{
		command_goodies(irc_conn, line, len, GOODIES_TEA);
	}
	// !help
	else if (strncmp(content, "!help", 5) == 0)
	{
		help_system_query("?? help");
	}
	/*
	 * Fun with !roulette
	 * You have to register your bot with nickserv and add it to the access-list
	 * of your channel to make the !roulette-command work.
	 * This is just tested on FreeNode. Please feel free to write patches, that
	 * will make this work on other Networks.
	 */
	else if (strncmp(content, "!roulette", 9) == 0)
	{
		static gint32 rand = -1;
		static gint bullets_fired = 0;

		if (rand == -1)
			rand = g_random_int() % 6;
		if (rand == bullets_fired)
		{
			irc_send_message(irc_conn, NULL, "*bang*");
			irc_kick(irc_conn, get_nickname(line, len));
			rand = g_random_int() % 6;
			bullets_fired = 0;
		}

		else
		{
			irc_send_message(irc_conn, NULL, "*click*");
			bullets_fired++;
		}
	}
	// !learn
	/// TODO require op privileges for !learn
	else if (strncmp(content, "!learn", 6) == 0)
	{
		command_learn(irc_conn, line, len);
	}
	// !alias
	else if (strncmp(content, "!alias", 6) == 0)
	{
		command_alias(irc_conn, line, len);
	}
}


static gboolean delayed_quit(gpointer data)
{
	main_quit();
	return FALSE;
}


static gboolean process_line(irc_conn_t *irc_conn, const gchar *line, guint len)
{
	static gchar msg[1024];
	guint msg_len;
	gint response = get_response(line, len);
	static gchar tmp_userlist[1024];
 	gchar *priv_sender;
 	const gchar *content;
	static gboolean connected = FALSE;

	// don't log the NAMES command's output (prevent log file spam)
	if (response != 353 && response != 366)
		irc_log(irc_conn, line, FALSE);

	// An error occurred, try to quit cleanly and print the error
	if ((response > 400 && response < 503))
	{
		// ignore Freenode's info messages sent with error code 477
		// (see http://freenode.net/faq.shtml#freenode-info)
		if (response != 477 || strstr(line, "[freenode-info]") == NULL)
		{
#ifndef DEBUG
			syslog(LOG_WARNING, "received error: %d (%s)", response, g_strstrip((gchar*) line));
#else
			g_print("Error: %s", line);
#endif
			g_free(irc_conn->quit_msg);
			irc_conn->quit_msg = g_strdup("I got an error and better leave in advance. Bye.");
			irc_goodbye(irc_conn);
			g_timeout_add_seconds(1, delayed_quit, NULL);
			return FALSE;
		}
	}

	if (! connected)
	{
		if (response == 376)
			connected = TRUE;
		else
			return TRUE;
	}

	content = irc_get_message(line, len);

	// retrieve user name list
	if (response == 353)
	{
		if (tmp_userlist[0] == '\0')
			g_strlcpy(tmp_userlist, strchr(content, ':') + 1, sizeof(tmp_userlist));
		else
			g_strlcat(tmp_userlist, strchr(content, ':') + 1, sizeof(tmp_userlist));
	}
	// retrieve end of user name list
	else if (response == 366)
	{
		if (tmp_userlist[0] != '\0')
		{
			set_user_list(tmp_userlist);
			tmp_userlist[0] = '\0';
		}
	}
	else if (! connected)
	{
		// don't do anything else until we got finished connecting (to skip MOTD messages)
	}
	// PING-PONG
	else if (strncmp("PING :", line, 6) == 0)
	{
		msg_len = g_snprintf(msg, sizeof msg, "PONG %s\r\n", line + 6); // 7 = "PING :"
		debug("PONG: -%s-\n", msg);
		irc_send(irc_conn, msg, msg_len);
	}
 	// handle private message
	else if ((priv_sender = get_command_sender(line, len, "PRIVMSG")) != NULL)
	{
		// to be able to send private messages to users, you need to register your bot's
		// nickname with Nickserv (at least on Freenode)
		irc_send_private_message(irc_conn, priv_sender, "I don't like private messages!");
	}
 	// handle kicks (we were kicked, bastards)
	else if (get_command_sender(line, len, "KICK") != NULL)
	{
		if (config->auto_rejoin)
		{
			g_usleep(5000000); // just wait a little bit
			msg_len = g_snprintf(msg, sizeof msg, "JOIN #%s\r\n", config->channel);
			irc_send(irc_conn, msg, msg_len);
		}
		else
		{
			main_quit();
		}
	}
	// Hi /me, acts on "hello $nickname" and "hi $nickname", hi and hello are case-insensitive
	// Thanks /me
	else if (strstr(content, config->nickname) != NULL)
	{
		const gchar *tmp_msg = irc_get_message_with_name(line, len, config->nickname);

		if (strncasecmp("hi", content, 2) == 0 || strncasecmp("hello", content, 5) == 0 || strncasecmp("hey", content, 3) == 0 ||
			strcasecmp(", hi", tmp_msg) == 0 || strcasecmp(", hello", tmp_msg) == 0 || strcasecmp(", hey", tmp_msg) == 0 ||
			strcasecmp(": hi", tmp_msg) == 0 || strcasecmp(": hello", tmp_msg) == 0 || strcasecmp(": hey", tmp_msg) == 0)
		{
			irc_send_message(irc_conn, NULL,
	"Hi %s. My name is %s and I'm here to offer additional services to you! "
	"Try \"?? help\" for general information and \"?? vomak\" for information about me.",
				get_nickname(line, len), config->nickname);
 		}
		else if (strncasecmp("thanks", content, 6) == 0 || strncasecmp("thx", content, 3) == 0 ||
				 strcasecmp(", thanks", tmp_msg) == 0 || strcasecmp(", thx", tmp_msg) == 0 ||
				 strcasecmp(": thanks", tmp_msg) == 0 || strcasecmp(": thx", tmp_msg) == 0)
 		{
			irc_send_message(irc_conn, get_nickname(line, len),
				"no problem. It was a pleasure to serve you.");
 		}
	}
	// ?? ...
	else if (strncmp(content, "?? ", 3) == 0)
	{
		help_system_query(content);
	}
	// pass to process_command() to process other commands (!beer, !test, !learn, ...)
	else if (*content == '!')
	{
		process_command(irc_conn, line, len, content);
	}

	return TRUE;
}


/*
 * Please note that this will not work on Networks without ChanServ, e.g. on
 * Quakenet or IRCnet. Your Bot has to be registered with NickServ and to be
 * added to the channel access list for this to work.
 */
static gboolean irc_toggle_op(irc_conn_t *irc_conn, gboolean request_op)
{
	const gchar *cmd;
	static gchar msg[1024];
	guint msg_len;

	if (irc_is_user_op(irc_conn, "ChanServ"))
	{
		cmd = (request_op) ? "op" : "deop";
		msg_len = g_snprintf(msg, sizeof msg, "PRIVMSG ChanServ :%s #%s\r\n", cmd, config->channel);
		irc_send(irc_conn, msg, msg_len);

		return TRUE;
	}
	return FALSE; /* it seems we don't have a ChanServ bot */
}


static gboolean irc_is_user_op(irc_conn_t *irc_conn, const gchar *nickname)
{
	const gchar *pos;
	const gchar *userlist;

	if (nickname == NULL)
		return FALSE;

	userlist = get_user_list();

	if ( (pos = strstr(userlist, nickname)) )
	{
		if ( (pos - 1) >= userlist && (*(pos - 1) == '@') )
		{
			return TRUE;
		}
	}
	else
	{
#ifdef DEBUG
		irc_send_message(irc_conn, NULL,
				"Hey. There are crazy things going on here. (O.o)");
#else
		syslog(LOG_WARNING, "user %s not found in names list of #%s", nickname, config->channel);
#endif
	}

	return FALSE;
}


static void irc_kick(irc_conn_t *irc_conn, const gchar *nickname)
{
	static gchar msg[1024];
	gboolean need_deop = FALSE;
	guint msg_len;

	TRACE

	if (! irc_is_user_op(irc_conn, config->nickname))
	{
		// if irc_toggle_op fails, most probably we don't have a ChanServ and at this point we
		// know we are not an op, so fail silently and don't try to kick
		if (! irc_toggle_op(irc_conn, TRUE)) /// TODO: prüfen, ob das auch erfolreich war
			return;
		need_deop = TRUE;
	}

	// give the server a chance to set the op status for us before we make us of it,
	// and let the victim read his *bang* message
	g_usleep(2000000);

	msg_len = g_snprintf(msg, sizeof msg, "KICK #%s %s\r\n", config->channel, nickname);
	irc_send(irc_conn, msg, msg_len);
	if (need_deop)
		irc_toggle_op(irc_conn, FALSE);
}


static gboolean input_cb(GIOChannel *source, GIOCondition cond, gpointer data)
{
#if 1
	gchar buf[1024];
	guint buf_len;
	irc_conn_t *irc = data;
	gboolean ret = TRUE;

	if (cond & (G_IO_ERR | G_IO_HUP))
		return FALSE;

	if ((buf_len = socket_fd_gets(irc->socket, buf, sizeof(buf))) != -1)
	{
		ret = process_line(irc, buf, buf_len);
	}
#else
	gsize buf_len;
	irc_conn_t *irc = data;

	if (cond & (G_IO_IN | G_IO_PRI))
	{
		gchar *buf = NULL;
		GIOStatus rv;
		GError *err = NULL;

		do
		{
			rv = g_io_channel_read_line(source, &buf, &buf_len, NULL, &err);
			if (buf != NULL)
			{
				buf_len -= 2;
				buf[buf_len] = '\0'; // skip trailing \r\n

				process_line(irc, buf, buf_len);
				g_free(buf);
			}
			if (err != NULL)
			{
				debug("%s: error: %s", __func__, err->message);
				g_error_free(err);
				err = NULL;
			}
		}
		while (rv == G_IO_STATUS_NORMAL || rv == G_IO_STATUS_AGAIN);
		debug("%s: status %d\n", __func__, rv);
	}
#endif
	return ret;
}


void irc_send_message(irc_conn_t *irc_conn, const gchar *target, const gchar *format, ...)
{
	static gchar tmp_msg[1024];
	static gchar msg[1024];
	guint msg_len;
	va_list ap;

	va_start(ap, format);
	g_vsnprintf(tmp_msg, sizeof tmp_msg, format, ap);
	va_end(ap);

	if (target)
		msg_len = g_snprintf(msg, sizeof msg, "PRIVMSG #%s :%s, %s\r\n", config->channel, target, tmp_msg);
	else
		msg_len = g_snprintf(msg, sizeof msg, "PRIVMSG #%s :%s\r\n", config->channel, tmp_msg);

	irc_send(irc_conn, msg, msg_len);
}


/*
 * target should not be NULL!
 */
void irc_send_notice(irc_conn_t *irc_conn, const gchar *target, const gchar *format, ...)
{
	static gchar tmp_msg[1024];
	static gchar msg[1024];
	guint msg_len;
	va_list ap;

	va_start(ap, format);
	g_vsnprintf(tmp_msg, sizeof tmp_msg, format, ap);
	va_end(ap);

	msg_len = g_snprintf(msg, sizeof msg, "NOTICE #%s :%s, %s\r\n", config->channel, target, tmp_msg);

	irc_send(irc_conn, msg, msg_len);
}


// simple wrapper for send() to enable logging for sent commands
gint irc_send(irc_conn_t *irc_conn, const gchar *msg, guint msg_len)
{
	irc_log(irc_conn, msg, TRUE);
	return send(irc_conn->socket, msg, msg_len, MSG_NOSIGNAL);
}


void irc_goodbye(irc_conn_t *irc)
{
	guint len;
	gchar msg[256];

	if (NZV(irc->quit_msg))
		len = g_snprintf(msg, sizeof msg, "QUIT :%s\r\n", irc->quit_msg);
	else
		len = g_strlcpy(msg, "QUIT :Good bye. It was a pleasure to serve you\r\n", sizeof msg);
	irc_send(irc, msg, len);
}


void irc_logfile_reopen(irc_conn_t *irc_conn)
{
	TRACE

	if (irc_conn->log_fd != NULL)
		fclose(irc_conn->log_fd);

	if (NZV(config->logfile))
	{
		irc_conn->log_fd = g_fopen(config->logfile, "a");
		if (! irc_conn->log_fd)
			vomak_warning("Logfile could not be opened.");
	}
}


gint irc_finalize(irc_conn_t *irc_conn)
{
	if (irc_conn->socket < 0)
		return -1;

	if (irc_conn->lock_tag > 0)
		g_source_remove(irc_conn->lock_tag);

	if (irc_conn->log_fd != NULL)
	{
		irc_log(irc_conn, "Stop logging\r\n", FALSE);
		fclose(irc_conn->log_fd);
	}

	if (irc_conn->read_ioc)
	{
		g_io_channel_shutdown(irc_conn->read_ioc, TRUE, NULL);
		g_io_channel_unref(irc_conn->read_ioc);
		irc_conn->read_ioc = NULL;
	}
	socket_fd_close(irc_conn->socket);
	irc_conn->socket = -1;

	g_free(irc_conn->quit_msg);

	return 0;
}


void irc_connect(irc_conn_t *irc_conn)
{
	struct hostent *he;
	struct sockaddr_in their_addr;
	gchar msg[256];
	guint msg_len;

	TRACE

	// Connect the socket to the server
	if ((he = gethostbyname(config->server)) == NULL)
	{
		perror("gethostbyname");
		exit(1);
	}

	if ((irc_conn->socket = socket(PF_INET, SOCK_STREAM, 0)) == -1)
	{
		perror("socket");
		exit(1);
	}

	their_addr.sin_family = PF_INET;
	their_addr.sin_port = htons(6667);
	their_addr.sin_addr = *((struct in_addr *)he->h_addr_list[0]);
	memset(&(their_addr.sin_zero), '\0', 8);

	if (connect(irc_conn->socket, (struct sockaddr *)&their_addr, sizeof(struct sockaddr)) == -1)
	{
		perror("connect");
		exit(1);
	}
	// Logging
	if (NZV(config->logfile))
	{
		irc_conn->log_fd = g_fopen(config->logfile, "a");
		if (irc_conn->log_fd)
			irc_log(irc_conn, "Start logging\r\n", FALSE);
		else
			vomak_warning("Logfile could not be opened.");
	}
	// say who we are
	msg_len = g_snprintf(msg, sizeof(msg), "USER %s %s %s :%s\r\n",
			config->username, config->servername, config->servername, config->realname);
	if (irc_send(irc_conn, msg, msg_len) == -1)
	{
		perror("send USER");
	}
	// and how we are called
	msg_len = g_snprintf(msg, sizeof(msg), "NICK %s\r\n", config->nickname);
	if (irc_send(irc_conn, msg, msg_len) == -1)
	{
		perror("send NICK");
	}
	// identify our nick
	if (NZV(config->nickserv_password))
	{
		msg_len = g_snprintf(msg, sizeof(msg), "PRIVMSG nickserv :identify %s\r\n", config->nickserv_password);
		// don't use irc_send() here, no need to log our password
		if (send(irc_conn->socket, msg, msg_len, MSG_NOSIGNAL) == -1)
		{
			perror("send NICKSERV");
		}
	}
	// join the channel
	msg_len = g_snprintf(msg, sizeof msg, "JOIN #%s\r\n", config->channel);
	if (irc_send(irc_conn, msg, msg_len) == -1)
	{
		perror("send");
	}

	// input callback, attached to the main loop
	irc_conn->read_ioc = g_io_channel_unix_new(irc_conn->socket);
	//~ g_io_channel_set_flags(irc_conn.read_ioc, G_IO_FLAG_NONBLOCK, NULL);
	g_io_channel_set_encoding(irc_conn->read_ioc, NULL, NULL);
	irc_conn->lock_tag = g_io_add_watch(irc_conn->read_ioc, G_IO_IN|G_IO_PRI|G_IO_ERR, input_cb, irc_conn);
}
