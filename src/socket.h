/*
 *      socket.h -  - this file is part of vomak - a very simple IRC bot
 *
 *      Copyright 2008 Enrico Tröger <enrico(dot)troeger(at)uvena(dot)de>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; version 2 of the License.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */


#ifndef SOCKET_H
#define SOCKET_H 1


typedef struct
{
	gchar		*file_name;
	GIOChannel	*read_ioc;
	gint 		 lock_socket;
	gint 		 lock_socket_tag;
} socket_info_t;


void socket_init	(socket_info_t *si, const gchar *filename);
gint socket_fd_close(gint sock);
gint socket_fd_gets (gint sock, gchar *buf, gint len);
gint socket_finalize(socket_info_t *si);


#endif
