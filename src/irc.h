/*
 *      irc.h - this file is part of vomak - a very simple IRC bot
 *
 *      Copyright 2008 Enrico Tröger <enrico(dot)troeger(at)uvena(dot)de>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; version 2 of the License.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */


#ifndef IRC_H
#define IRC_H 1



typedef struct
{
	gint		 socket;
	GIOChannel	*read_ioc;
	gint 		 lock_tag;
	FILE		*log_fd;
	gchar		*quit_msg;
} irc_conn_t;


void irc_goodbye			(irc_conn_t *irc);
gint irc_finalize			(irc_conn_t *irc_conn);
void irc_connect			(irc_conn_t *irc_conn);
void irc_send_message		(irc_conn_t *irc_conn, const gchar *target, const gchar *format, ...);
void irc_send_notice		(irc_conn_t *irc_conn, const gchar *target, const gchar *format, ...);
gint irc_send				(irc_conn_t *irc_conn, const gchar *msg, guint msg_len);
const gchar *irc_get_message(const gchar *line, guint len);
gboolean irc_query_names	(gpointer data);
void irc_logfile_reopen		(irc_conn_t *irc_conn);

#endif
