/*
 *      vomak.c - this file is part of vomak - a very simple IRC bot
 *
 *      Copyright 2008 Enrico Tröger <enrico(dot)troeger(at)uvena(dot)de>
 *      Copyright 2008 Dominic Hopf <dh(at)dmaphy(dot)de>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; version 2 of the License.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */



#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#ifndef DEBUG
# include <syslog.h>
# include <stdarg.h>
#endif

#include <glib.h>
#include <glib/gstdio.h>

#include "vomak.h"
#include "socket.h"
#include "irc.h"

#define CONFIG_SECTION_GENERAL "general"
#define CONFIG_SECTION_IRC "irc"
#define CONFIG_SECTION_EXTERNALS "externals"
#define CONFIG_SECTION_DATABASE "general"


static irc_conn_t irc_conn;
static socket_info_t socket_info;
static GMainLoop *main_loop = NULL;
static gchar *userlist = NULL;
static GList *words = NULL;
config_t *config;

static void load_database_real(GKeyFile *keyfile)
{
	gsize j, len_keys = 0;
	gchar **keys;

	TRACE

	// unload previously loaded data, aka reload
	if (config->data != NULL)
	{
		g_hash_table_destroy(config->data);
	}

	config->data = g_hash_table_new_full(g_str_hash, g_str_equal, g_free, g_free);

	keys = g_key_file_get_keys(keyfile, CONFIG_SECTION_DATABASE, &len_keys, NULL);
	for (j = 0; j < len_keys; j++)
	{
		g_hash_table_insert(config->data, g_utf8_strdown(keys[j], -1),
					g_key_file_get_string(keyfile, CONFIG_SECTION_DATABASE, keys[j], NULL));
	}
	g_strfreev(keys);
}


static void load_database()
{
	gchar *config_name = g_build_filename(getenv("HOME"), ".vomak", "database", NULL);
	GKeyFile *keyfile = g_key_file_new();

	TRACE

	g_key_file_load_from_file(keyfile, config_name, G_KEY_FILE_NONE, NULL);

	load_database_real(keyfile);

	g_key_file_free(keyfile);
	g_free(config_name);
}


static void signal_cb(gint sig)
{
	if (sig == SIGTERM || sig == SIGINT)
	{
		gchar *signame;

		switch (sig)
		{
			case SIGTERM: signame = "SIGTERM"; break;
			case SIGINT: signame = "SIGINT"; break;
			case SIGPIPE: signame = "SIGPIPE"; break;
			default: signame = "unknown"; break;
		}
		debug("Received %s, cleaning up", signame);

		irc_goodbye(&irc_conn);

		main_quit();
	}
	if (sig == SIGHUP)
		irc_logfile_reopen(&irc_conn);
}


void main_quit()
{
#ifndef DEBUG
	closelog();
#endif
	irc_finalize(&irc_conn);
	socket_finalize(&socket_info);

	g_main_loop_quit(main_loop);
}


static gboolean socket_input_cb(GIOChannel *ioc, GIOCondition cond, gpointer data)
{
	gint fd, sock;
	static gchar buf[1024];
	static gchar msg[1024];
	struct sockaddr_in caddr;
	guint caddr_len, len, msg_len;

	caddr_len = sizeof(caddr);

	fd = g_io_channel_unix_get_fd(ioc);
	sock = accept(fd, (struct sockaddr *)&caddr, &caddr_len);

	TRACE
	while ((len = socket_fd_gets(sock, buf, sizeof(buf))) != -1)
	{
		if (strncmp("quit", buf, 4) == 0)
		{
			g_free(irc_conn.quit_msg);
			irc_conn.quit_msg = g_strdup(g_strchomp(buf + 5));
			signal_cb(SIGTERM);
		}
		else if (strncmp("reload", buf, 6) == 0)
		{
			load_database();
		}
		else if (strncmp("openlog", buf, 7) == 0)
		{
			irc_logfile_reopen(&irc_conn);
		}
		else if (strncmp("say", buf, 3) == 0)
		{
			irc_send_message(&irc_conn, NULL, g_strchomp(buf + 4));
		}
		else if (strncmp("userlist", buf, 8) == 0)
		{
			if (userlist)
			{
				send(sock, userlist, strlen(userlist), MSG_NOSIGNAL);
				send(sock, "\n", 1, MSG_NOSIGNAL);
			}
			else
				send(sock, "error\n", 6, MSG_NOSIGNAL);
		}
		else
		{
			msg_len = g_snprintf(msg, sizeof msg, "%s\r\n", buf);
			irc_send(&irc_conn, msg, msg_len);
			vdebug(msg);
		}
	}
	socket_fd_close(sock);

	return TRUE;
}


static void hash_add_to_list(gpointer key, gpointer value, gpointer user_data)
{
	if (key != NULL)
	{
		words = g_list_insert_sorted(words, key, (GCompareFunc) strcmp);
	}
}


static gint write_file(const gchar *filename, const gchar *text)
{
	FILE *fp;
	gint bytes_written, len;

	if (filename == NULL)
	{
		return ENOENT;
	}

	len = strlen(text);

	fp = g_fopen(filename, "w");
	if (fp != NULL)
	{
		bytes_written = fwrite(text, sizeof (gchar), len, fp);
		fclose(fp);

		if (len != bytes_written)
		{
			debug("written only %d bytes, had to write %d bytes to %s",	bytes_written, len, filename);
			return EIO;
		}
	}
	else
	{
		debug("could not write to file %s (%s)", filename, g_strerror(errno));
		return errno;
	}
	return 0;
}


void help_system_query(const gchar *msg)
{
	gchar *result;
	gchar *lowered_keyword;

	if (strncmp("?? ", msg, 3) != 0)
		return;

	msg += 3;

	lowered_keyword = g_utf8_strdown(msg, -1);

	if (strncmp("keywords", lowered_keyword, 8) == 0)
	{
		GList *item;
		GString *str = g_string_sized_new(256);

		words = NULL;

		g_hash_table_foreach(config->data, hash_add_to_list, NULL);

		for (item = words; item != NULL; item = g_list_next(item))
		{
			g_string_append(str, item->data);
			g_string_append_c(str, ' ');
		}

		irc_send_message(&irc_conn, NULL, "%s: %s", msg, str->str);
		g_list_free(words);
		g_string_free(str, TRUE);
	}
	else
	{
		result = g_hash_table_lookup(config->data, lowered_keyword);

		/*
		 * Look for the @-Char in the result string. If one is found, most
		 * likely the !alias-command was used and this has to be resolved
		 * recursively until no @ appears anymore in the string.
		 */
		while (result && result[0] == '@')
			result = g_hash_table_lookup(config->data, result + 1);

		if (result)
			irc_send_message(&irc_conn, NULL, "%s: %s", msg, result);
	}
	g_free(lowered_keyword);
}


// return value:
//  0  - added
//  1  - updated
// -1  - error
gint help_system_learn(const gchar *keyword, const gchar *text)
{
	gchar *config_name;
	gchar *data;
	gchar *key;
	gint ret = 0;
	GKeyFile *keyfile;

	TRACE

	if (keyword == NULL || text == NULL)
		return -1;

	config_name = g_build_filename(getenv("HOME"), ".vomak", "database", NULL);
	keyfile = g_key_file_new();

	g_key_file_load_from_file(keyfile, config_name, G_KEY_FILE_NONE, NULL);

	key = g_hash_table_lookup(config->data, keyword);
	if (key != NULL)
		ret = 1; // if key is non-NULL it is already available and gets updated

	g_key_file_set_string(keyfile, CONFIG_SECTION_DATABASE, keyword, text);

	data = g_key_file_to_data(keyfile, NULL, NULL);
	write_file(config_name, data);

	load_database_real(keyfile);

	g_key_file_free(keyfile);
	g_free(config_name);
	g_free(data);

	return ret;
}


static void config_free()
{
	TRACE
	g_free(config->socketname);
	g_free(config->socketperm);
	g_free(config->server);
	g_free(config->channel);
	g_free(config->servername);
	g_free(config->nickname);
	g_free(config->username);
	g_free(config->realname);
	g_free(config->nickserv_password);
	g_free(config->logfile);
	g_free(config->fortune_cmd);
	if (config->data != NULL)
		g_hash_table_destroy(config->data);
	g_free(config);
	config = NULL;
}


static config_t *config_read()
{
	GKeyFile *keyfile = g_key_file_new();
	gchar *config_name = g_build_filename(getenv("HOME"), ".vomak", "config", NULL);
	config_t *config = g_new0(config_t, 1);

	TRACE

	g_key_file_load_from_file(keyfile, config_name, G_KEY_FILE_NONE, NULL);

	config->socketname = g_key_file_get_string(keyfile, CONFIG_SECTION_GENERAL, "socketname", NULL);
	config->socketperm = g_key_file_get_string(keyfile, CONFIG_SECTION_GENERAL, "socketperm", NULL);
	config->logfile = g_key_file_get_string(keyfile, CONFIG_SECTION_GENERAL, "logfile", NULL);
	config->query_names_interval = g_key_file_get_integer(keyfile, CONFIG_SECTION_GENERAL, "query_names_interval", NULL);

	config->server = g_key_file_get_string(keyfile, CONFIG_SECTION_IRC, "server", NULL);
	config->port = g_key_file_get_integer(keyfile, CONFIG_SECTION_IRC, "port", NULL);
	config->channel = g_key_file_get_string(keyfile, CONFIG_SECTION_IRC, "channel", NULL);
	config->servername = g_key_file_get_string(keyfile, CONFIG_SECTION_IRC, "servername", NULL);
	config->nickname = g_key_file_get_string(keyfile, CONFIG_SECTION_IRC, "nickname", NULL);
	config->username = g_key_file_get_string(keyfile, CONFIG_SECTION_IRC, "username", NULL);
	config->realname = g_key_file_get_string(keyfile, CONFIG_SECTION_IRC, "realname", NULL);
	config->nickserv_password = g_key_file_get_string(keyfile, CONFIG_SECTION_IRC, "nickserv_password", NULL);
	config->auto_rejoin = g_key_file_get_boolean(keyfile, CONFIG_SECTION_IRC, "auto_rejoin", NULL);

	config->fortune_cmd = g_key_file_get_string(keyfile, CONFIG_SECTION_EXTERNALS, "fortune_cmd", NULL);

	g_key_file_free(keyfile);
	g_free(config_name);

	// if anything could not be read, abort. We don't use default values
	if (config->socketname == NULL ||
		config->socketperm == NULL ||
		config->server == NULL ||
		config->channel == NULL ||
		config->servername == NULL ||
		config->nickname == NULL ||
		config->username == NULL ||
		config->realname == NULL)
	{
		fprintf(stderr, "Config file does not exist or is not complete.\n");
		exit(1);
	}

	return config;
}


static void init_socket()
{
	TRACE

    /* create unix domain socket for remote operation */
	socket_info.lock_socket = -1;
	socket_info.lock_socket_tag = 0;
	socket_init(&socket_info, config->socketname);

	if (socket_info.lock_socket >= 0)
	{
		mode_t mode;
		sscanf(config->socketperm, "%o", &mode);
		/// FIXME for some reason the sticky bit is set when socketperm is "0640"
		chmod(config->socketname, mode);
		socket_info.read_ioc = g_io_channel_unix_new(socket_info.lock_socket);
		g_io_channel_set_flags(socket_info.read_ioc, G_IO_FLAG_NONBLOCK, NULL);
		socket_info.lock_socket_tag = g_io_add_watch(socket_info.read_ioc,
						G_IO_IN|G_IO_PRI|G_IO_ERR, socket_input_cb, NULL);
	}
}


void set_user_list(const gchar *list)
{
	g_free(userlist);
	userlist = g_strdup(list);
}


const gchar *get_user_list()
{
	return userlist;
}


gint main(gint argc, gchar **argv)
{
	irc_conn.quit_msg = NULL;

	// init stuff
	main_loop = g_main_loop_new(NULL, FALSE);
	config = config_read();
	load_database();
	init_socket();
	irc_connect(&irc_conn);
	signal(SIGTERM, signal_cb);
	signal(SIGINT, signal_cb);

	if (config->query_names_interval)
		g_timeout_add(config->query_names_interval * 60000, irc_query_names, &irc_conn);

	// if not a debug build, deattach from console and go into background
#ifndef DEBUG
	signal(SIGTTOU, SIG_IGN);
	signal(SIGTTIN, SIG_IGN);
	signal(SIGTSTP, SIG_IGN);
	openlog("vomak", LOG_PID, LOG_DAEMON);

	if (daemon(1, 0) < 0)
	{
		g_printerr("Unable to daemonize\n");
		return -1;
	}
#endif

	// main loop
	g_main_loop_run(main_loop);

	config_free();

	return 0;
}


/* if debug mode is enabled, print the warning to stderr, else log it with syslog */
void vomak_warning(gchar const *format, ...)
{
	va_list args;
	va_start(args, format);

#ifdef DEBUG
	g_logv(G_LOG_DOMAIN, G_LOG_LEVEL_WARNING, format, args);
#else
	vsyslog(LOG_WARNING, format, args);
#endif

	va_end(args);
}

